#ifndef PLAYSCENE_H
#define PLAYSCENE_H

#include <QMainWindow>
#include<player.h>
#include<soil.h>
#include<npc.h>
#include<QKeyEvent>
#include<QMediaPlayer>

#include<ps5.h>
namespace Ui {
class PlayScene;
}

class PlayScene : public QMainWindow
{
    Q_OBJECT

public:
    explicit PlayScene(QWidget *parent = nullptr);
    ~PlayScene();
    Player *myself;
    soil *tudi[20][20];
void keyPressEvent(QKeyEvent *ev);
  int score;
  void paintSky();
  void paintBackofSoil();
  void paintBloodBar();
void paintSoilandMine();//绘制挖的矿
NPC *Tutu;
QLabel* scoreLabel;
void paintEvent(QPaintEvent* ev);
void keyReleaseEvent(QKeyEvent *ev);
void movingNPC();
void nextLevel();
void gameover();
int t;
bool ans;QSound *beattack;
 QSound *bgm;
ps5 *nextlevel;


private:
    Ui::PlayScene *ui;
};

#endif // PLAYSCENE_H
