#ifndef NPC_H
#define NPC_H

#include <QWidget>
#include<QLabel>

class NPC : public QLabel
{
    Q_OBJECT
public:
    explicit NPC(QWidget *parent = nullptr);
    NPC(QString app);
    int shengming;
    QString appearance;

    void speak();
    void beAttack();
    void attack();
signals:



};

#endif // NPC_H
