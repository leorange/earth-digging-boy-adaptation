/********************************************************************************
** Form generated from reading UI file 'playscene.ui'
**
** Created by: Qt User Interface Compiler version 6.2.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_PLAYSCENE_H
#define UI_PLAYSCENE_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QProgressBar>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_PlayScene
{
public:
    QWidget *centralwidget;
    QProgressBar *progressBar;
    QLabel *npcName;
    QProgressBar *npcBlood;
    QLabel *scoree;
    QMenuBar *menubar;
    QStatusBar *statusbar;

    void setupUi(QMainWindow *PlayScene)
    {
        if (PlayScene->objectName().isEmpty())
            PlayScene->setObjectName(QString::fromUtf8("PlayScene"));
        PlayScene->resize(752, 769);
        PlayScene->setBaseSize(QSize(600, 600));
        centralwidget = new QWidget(PlayScene);
        centralwidget->setObjectName(QString::fromUtf8("centralwidget"));
        progressBar = new QProgressBar(centralwidget);
        progressBar->setObjectName(QString::fromUtf8("progressBar"));
        progressBar->setGeometry(QRect(190, 30, 120, 23));
        progressBar->setValue(24);
        npcName = new QLabel(centralwidget);
        npcName->setObjectName(QString::fromUtf8("npcName"));
        npcName->setGeometry(QRect(440, 30, 69, 20));
        npcBlood = new QProgressBar(centralwidget);
        npcBlood->setObjectName(QString::fromUtf8("npcBlood"));
        npcBlood->setGeometry(QRect(520, 30, 120, 23));
        npcBlood->setValue(24);
        scoree = new QLabel(centralwidget);
        scoree->setObjectName(QString::fromUtf8("scoree"));
        scoree->setGeometry(QRect(350, 370, 69, 20));
        PlayScene->setCentralWidget(centralwidget);
        menubar = new QMenuBar(PlayScene);
        menubar->setObjectName(QString::fromUtf8("menubar"));
        menubar->setGeometry(QRect(0, 0, 752, 26));
        PlayScene->setMenuBar(menubar);
        statusbar = new QStatusBar(PlayScene);
        statusbar->setObjectName(QString::fromUtf8("statusbar"));
        PlayScene->setStatusBar(statusbar);

        retranslateUi(PlayScene);

        QMetaObject::connectSlotsByName(PlayScene);
    } // setupUi

    void retranslateUi(QMainWindow *PlayScene)
    {
        PlayScene->setWindowTitle(QCoreApplication::translate("PlayScene", "MainWindow", nullptr));
        npcName->setText(QCoreApplication::translate("PlayScene", "HuTutu", nullptr));
        scoree->setText(QCoreApplication::translate("PlayScene", "SCORE:", nullptr));
    } // retranslateUi

};

namespace Ui {
    class PlayScene: public Ui_PlayScene {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_PLAYSCENE_H
