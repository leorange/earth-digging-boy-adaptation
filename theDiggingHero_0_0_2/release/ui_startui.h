/********************************************************************************
** Form generated from reading UI file 'startui.ui'
**
** Created by: Qt User Interface Compiler version 6.2.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_STARTUI_H
#define UI_STARTUI_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_StartUi
{
public:
    QPushButton *actionQuit;

    void setupUi(QWidget *StartUi)
    {
        if (StartUi->objectName().isEmpty())
            StartUi->setObjectName(QString::fromUtf8("StartUi"));
        StartUi->resize(800, 600);
        actionQuit = new QPushButton(StartUi);
        actionQuit->setObjectName(QString::fromUtf8("actionQuit"));
        actionQuit->setGeometry(QRect(220, 490, 171, 71));

        retranslateUi(StartUi);

        QMetaObject::connectSlotsByName(StartUi);
    } // setupUi

    void retranslateUi(QWidget *StartUi)
    {
        StartUi->setWindowTitle(QCoreApplication::translate("StartUi", "StartUi", nullptr));
        actionQuit->setText(QCoreApplication::translate("StartUi", "QUIT", nullptr));
    } // retranslateUi

};

namespace Ui {
    class StartUi: public Ui_StartUi {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_STARTUI_H
