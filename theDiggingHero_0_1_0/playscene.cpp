#include "playscene.h"
#include "ui_playscene.h"
#include<QLabel>
#include"soil.h"
#include<QProgressBar>
#include<QPainter>
#include<QTimer>
#include<math.h>
#include<QDebug>
#include<QPropertyAnimation>
#include<mypushbutton.h>
PlayScene::PlayScene(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::PlayScene)
{
     beattack=new QSound(":/res/RES/daji.wav",this);
     losem=new QSound(":/res/RES/losem.wav",this);
    ui->setupUi(this);
    ui->scoree->move(450,15);
    this->score =0;
    this->grabKeyboard();
    this->setFixedSize(600,600);
    this->setWindowIcon(QPixmap(":/res/RES/b5efa3e82300ed987d52f31ec973ab3.png"));
    this->setWindowTitle("The Digging Hero");
    //积分
    score=0;
    scoreLabel=new QLabel(this);
    scoreLabel->move(500,10);
    QTimer *scoreTimer=new QTimer(this);
    scoreTimer->start(200);
    connect(scoreTimer,&QTimer::timeout,[=](){

        scoreLabel->setText(QString::number(score));
    });
//bgm的设定
    bgm=new QSound(":/res/RES/bgm1.wav",this);
    bgm->play();
    bgm->setLoops(500);
    //游戏失败的标志
    lose=new QLabel;
   if(! losepix.load(":/res/RES/gameover.png"))
   {
   qDebug()<<"failed to load gameover!";

   };
   losepix=losepix.scaled(200,100);
   lose->setGeometry(0,0,losepix.width(),losepix.height());

    lose->setPixmap(losepix);
            lose->setParent(this);
      lose->move(0.5*(600-200),-100);
      exitt=new MyPushButton(":/res/RES/exit1.png",":/res/RES/exit1.png");
             exitt->setParent(this);
             exitt->move(300,-100);


    /////
    /////
    /// ///
    ///
    /// //
    ///
    ///


/////this->paintSky();
////渲染土壤背景
///this->paintBackofSoil();
//绘制土壤矿土
    for(int i=0;i<20;i++)
    {
        for(int j=0;j<20;j++)
        {
            if(j<=10)
            {
            soil *s=new soil("0");
            s->setParent(this);
            s->move(30*i,30*j);
            s->posX=i;
            s->posY=j;
            this->tudi[i][j]=s;

            }
            else
            {
             soil *s=new soil(":/res/RES/1soil.png");
             if(j>10)
             {
                 delete s;
                 s=new soil(":/res/RES/2soil.png");
             }
             s->setParent(this);
             s->move(30*i,30*j);
             s->posX=i;
             s->posY=j;
             this->tudi[i][j]=s;


            }

            if(i==12&&j==13)
            {
                tudi[i][j]->changeCloth(":/res/RES/4soil.png");
            }

            if(i==17&&j==19)
            {
                tudi[i][j]->changeCloth(":/res/RES/4soil.png");


            }

            if(i==2&&j==12)
            {
                tudi[i][j]->changeCloth(":/res/RES/4soil.png");


            }
            if(i==7&&j==16)
            {
                tudi[i][j]->changeCloth(":/res/RES/4soil.png");


            }





        }
    }
//创建玩家

myself=new Player(":/res/RES/1me.png");
myself->setParent(this);
myself->move(300,50);//他的脚在(x，y+50);
//创建一个NPC
Tutu=new NPC(":/res/RES/Tutu.PNG");
Tutu->setParent(this);
Tutu->move(300,0);
//    玩家血条
this->paintBloodBar();
//    实时更新
 QTimer *timer=new QTimer(this);//设定计时器
 timer->start(50);//计时器时间是0.5s
 connect(timer,&QTimer::timeout,[=](){
     int ii,jj;
     ii=(myself->x())/30;
     jj=(myself->y()+50)/30;
     if(tudi[ii][jj]->soilImg==":/res/RES/1soil.png"|tudi[ii][jj]->soilImg=="0")
     {
            int xx=myself->x();
            int yy=myself->y();
            myself->move(xx,yy);
            xx=myself->x();
            yy=myself->y()+2;
            myself->move(xx,yy);
            xx=myself->x();
            yy=myself->y()+2;
            myself->move(xx,yy);
            xx=myself->x();
            yy=myself->y()+2;
            myself->move(xx,yy);
            xx=myself->x();
            yy=myself->y()+2;
            myself->move(xx,yy);
            xx=myself->x();
            yy=myself->y()+2;
            myself->move(xx,yy);   
     }

});//信号连接槽，发出信号的是timer，信号是timeout,实时判断是否掉落并且安排掉落,实时监督NPC与玩家的位置并安排攻击

 //连接NPC的攻击与玩家掉血
  QTimer *TutuattackTimer=new QTimer(this);
  TutuattackTimer->start(1000);
  connect(TutuattackTimer,&QTimer::timeout,[=](){
      if(((Tutu->x()-myself->x()<=40)&&(Tutu->x()-myself->x()>=-40))&&(((Tutu->y()-myself->y()<=30)&&(Tutu->y()-myself->y()>=-30))))
      {
          myself->beAttacked();
          beattack->play();

      }
  });


 QTimer *TutuTimer=new QTimer(this);
 TutuTimer->start(50);
 connect(TutuTimer,&QTimer::timeout,[=](){

     //对于图图的重力效应
if(Tutu->y()>600)
{
    Tutu->shengming-=100;
}


          int mm=(Tutu->x())/30;
          int nn=(Tutu->y()+50)/30;
          if(tudi[mm][nn]->soilImg==":/res/RES/1soil.png"||tudi[mm][nn]->soilImg=="0")
          {
                 int xx=Tutu->x();
                 int yy=Tutu->y();
                 xx=Tutu->x();
                 yy=Tutu->y()+2;
                 Tutu->move(xx,yy);
                 xx=Tutu->x();
                 yy=Tutu->y()+2;
                 Tutu->move(xx,yy);
                 xx=Tutu->x();
                 yy=Tutu->y()+2;
                 Tutu->move(xx,yy);
                 xx=Tutu->x();
                 yy=Tutu->y()+2;
                 Tutu->move(xx,yy);
                 xx=Tutu->x();
                 yy=Tutu->y()+2;
                 Tutu->move(xx,yy);
          }

          if(Tutu->shengming<=0)
          {
              Tutu->close();
              ui->npcBlood->close();
              ui->npcName->close();
              TutuattackTimer->stop();
          }


 });





 connect(myself,&Player::attack,[=](){
     if(((Tutu->x()-myself->x()<=40)&&(Tutu->x()-myself->x()>=-40))&&(((Tutu->y()-myself->y()<=30)&&(Tutu->y()-myself->y()>=-30))))
     {
         this->Tutu->beAttack();
         QSound beattack(":/res/RES/daji.wav",this);
         beattack.play();
         qDebug()<<Tutu->shengming;

     }

 });


this->movingNPC();

this->nextLevel();
this->gameover();

}

void PlayScene::nextLevel()
{

    ///开启下一关
    QTimer *playthenext=new QTimer(this);
    playthenext->start(500);
    connect(playthenext,&QTimer::timeout,[=](){

        if(this->score>=300000)
        {
            bgm->stop();

            QLabel *win=new QLabel;
            QPixmap tmpix;
            tmpix.load(":/res/RES/loadingUi.jpg");
            win->setGeometry(0,0,600,600);
            win->setPixmap(tmpix);
            win->setParent(this);
            win->show();
            win->move(0,0);


    playthenext->stop();
    this->nextlevel=new ps5;
    QTimer::singleShot(2000,this,[=](){
    this->close();
    nextlevel->show();
    });
        }

    });











}
void PlayScene::gameover()
{

    QTimer *timee=new QTimer(this);
    timee->start(200);
    connect(timee,QTimer::timeout,[=](){

        qDebug()<<myself->shengming;

        if(myself->shengming<=2)
        {

            timee->stop();

            qDebug()<<"Game Over！！";

QPropertyAnimation *anii1=new QPropertyAnimation
        (lose,"geometry");
anii1->setDuration(1000);
anii1->setStartValue(QRect(lose->x(),lose->y(),lose->width(),lose->height()));
anii1->setEndValue(QRect(lose->x(),lose->y()+200,lose->width(),lose->height()));

anii1->setEasingCurve(QEasingCurve::OutBounce);
anii1->start();
            bgm->stop();
            losem->play();

        exitt->move(250,250);
        connect(exitt,&MyPushButton::clicked,[=](){

            this->close();
        });

        }





    });




}

PlayScene::~PlayScene()

{
    delete ui;

}


void PlayScene::keyPressEvent(QKeyEvent *ev)
{
    qDebug()<<"keyEvent in 1";
    if(ev->key()==Qt::Key_Right)
    {

        int xx=this->myself->x();
        int yy=this->myself->y();

        if(xx<590)
        {

            qDebug()<<xx<<"  "<<yy;
            int ii=xx/30;
            int jj=(yy)/30;
            qDebug()<<ii<<" "<<jj;
                if(tudi[ii][jj+1]->soilImg=="0"||tudi[ii][jj+1]->soilImg==":/res/RES/1soil.png")
                {
                    this->myself->move(xx+10,yy);
                    this->myself->step->play();
                this->myself->changeCloth(":/res/RES/r0me.png");

                QTimer::singleShot(1000,this,[=](){
                    if(ev->key()==Qt::Key_Right)
                    {
                this->myself->changeCloth(":/res/RES/r1me.png");
                    }
                });

                }
                else
                {
                    if(tudi[ii][jj]->soilImg==":/res/RES/4soil.png")
                    {
                        if(myself->shengming<=90)
                        {
                        myself->shengming+=10;
                        }

                    }


                    qDebug()<<"begin to dig the right";
                          this->score+=250;
                    this->myself->changeCloth(":/res/RES/digR0.png");
                    QTimer::singleShot(800,this,[=](){
                        this->tudi[ii][jj+1]->changeCloth(":/res/RES/1soil.png");
                    });

                    QTimer::singleShot(200,this,[=](){
                         this->myself->changeCloth(":/res/RES/1me.png");
                        this->score+=10;
                    });
                }


        }




        return;

    }
    if(ev->key()==Qt::Key_Left)
    {   int xx=this->myself->x();
        int yy=this->myself->y();
        if(xx>=10)
        {

        qDebug()<<xx<<"  "<<yy;
        int ii=xx/30;
        int jj=(yy)/30;
        qDebug()<<ii<<" "<<jj;
            if(tudi[ii][jj+1]->soilImg=="0"||tudi[ii][jj+1]->soilImg==":/res/RES/1soil.png")
            {
                this->myself->move(xx-10,yy);
                this->myself->step->play();
            this->myself->changeCloth(":/res/RES/l0me.png");
            QTimer::singleShot(1000,this,[=](){
                 if(ev->key()==Qt::Key_Left)
                 {
            this->myself->changeCloth(":/res/RES/l1me.png");
}
            });

            }
            else
            {

                qDebug()<<"begin to dig the left";
                      this->score+=250;
                this->myself->changeCloth(":/res/RES/digL0.png");
                QTimer::singleShot(800,this,[=](){
                    this->tudi[ii][jj+1]->changeCloth(":/res/RES/1soil.png");
                });


                    this->score+=10;

            }



        }

        return;
    }
    if(ev->key()==Qt::Key_Up)
    {
        int xx=this->myself->x();
        int yy=this->myself->y()-10;
        int ii=xx/30;
        int jj=(yy)/30;
        if(tudi[ii][jj]->soilImg=="0"|tudi[ii][jj]->soilImg==":/res/RES/1soil.png")
        {
        this->myself->changeCloth(":/res/RES/4me.png");
        this->myself->move(xx,yy);
        }
        return;
    }
    if(ev->key()==Qt::Key_Down)
    {

        int xx=this->myself->x();
        int yy=this->myself->y();
     int ii=xx/30;
     int jj=(yy+50)/30;

     if(this->tudi[ii][jj]->soilImg==":/res/RES/2soil.png"||tudi[ii][jj]->soilImg==":/res/RES/4soil.png")

     {
         if(tudi[ii][jj]->soilImg==":/res/RES/4soil.png")
         {
             if(myself->shengming<=90)
             {
             myself->shengming+=10;
             }

         }


               this->score+=250;
         this->myself->changeCloth(":/res/RES/3me.png");
         QTimer::singleShot(800,this,[=](){
             this->tudi[ii][jj]->changeCloth(":/res/RES/1soil.png");
             this->score+=10;
         });


     }
        return;
    }
    if(ev->key()==Qt::Key_Space)
    {
//        this->myself->beAttacked();
//        myself->shengming=1;
//        QSound beattack(":/res/RES/daji.wav",this);
//        beattack.play();
        return;
    }
    if(ev->key()==Qt::Key_Z)
    {

        if(((Tutu->x()-myself->x()<=40)&&(Tutu->x()-myself->x()>=-40))&&(((Tutu->y()-myself->y()<=30)&&(Tutu->y()-myself->y()>=-30))))
        {
            this->myself->attack();
            Tutu->beAttack();
            qDebug()<<"myself->attack()";
        }
        qDebug()<<myself->x()<<"  "<<myself->y();
        qDebug()<<Tutu->x()<<"    "<<Tutu->y();
    }
   return QMainWindow::keyPressEvent(ev);


}
void PlayScene::paintSky()
{
    QPainter painter(this);
        QPixmap sky;
        sky.load(":/res/RES/loadingUi.png");
    sky=sky.scaled(600,600);
    painter.drawPixmap(0,0,this->width(),this->height(),sky);
}
void PlayScene::paintBackofSoil(){
    QLabel *label[20][20];
 for(int i=0;i<20;i++)
 {
     for(int j=0;j<20;j++)
     {
         QLabel* labe=new QLabel(this);
         QPixmap tupix;
         tupix.load("0");
//                 (":/res/RES/3soil.png");
//         tupix=tupix.scaled(50,50);
         labe->setGeometry(0,0,30,30);
         labe->setPixmap(tupix);
         labe->move(30*i,30*j);
         label[i][j]=labe;
     }
 }
}
void PlayScene::paintBloodBar()
{

    ui->npcBlood->move(this->width()-10-ui->npcBlood->width(),40);
    ui->npcName->setText("HuTutu");
      ui->npcName->move(this->width()-10-ui->npcBlood->width()-45,40);

ui->progressBar->move(this->width()-10-ui->npcBlood->width(),65);

    //玩家血量条
     ui->progressBar->setMaximum(100);
     ui->progressBar->setValue(this->myself->shengming);
     ui->npcBlood->setMaximum(100);
     ui->npcBlood->setValue(this->Tutu->shengming);
     QTimer *bloodtime=new QTimer(this);
     bloodtime->start(500);
     connect(bloodtime,&QTimer::timeout,[=](){
         ui->progressBar->setValue(myself->shengming);
         ui->npcBlood->setValue(Tutu->shengming);
     });
}

void PlayScene::paintSoilandMine()
{

}
void PlayScene::movingNPC()
{

    QTimer *mNTimer=new QTimer(this);
    mNTimer->start(200);
this->t=0;
    this->ans=0;
    connect(mNTimer,&QTimer::timeout,[=](){

        if(Tutu->shengming<=0)
        {
            mNTimer->stop();
        }

        int x=Tutu->x();
        int y=Tutu->y();
        int ii=x/30;
        int jj=(y-10)/30;

            if(this->ans)
            {
                if((tudi[ii][jj+1]->soilImg=="0"||tudi[ii][jj+1]->soilImg==":/res/RES/1soil.png")&&(x>=5))
                {
            this->Tutu->move(x-5,y);
            }
                t--;
            }
            else
            {
                if((tudi[ii][jj+1]->soilImg=="0"||tudi[ii][jj+1]->soilImg==":/res/RES/1soil.png")&&(x<580))
                {
            this->Tutu->move(x+5,y);
                }
                t=t+1;
            }
            if(t>=30)
            {
               this-> ans=1;
            }
            if(t<=0)
            {
                this->ans=0;
            }

    });
}
void PlayScene::paintEvent(QPaintEvent *ev)
{    //创建画家，指定绘图设备
    QPainter painter(this);
    //创建QPixmap对象
    QPixmap pix=QPixmap(":/res/RES/bg2.jpg").scaled(this->size());
    //加载图片
    //绘制背景图
    painter.drawPixmap(0,0,this->width(),this->height(),pix);
}


void PlayScene::keyReleaseEvent(QKeyEvent *event)
{
    if(event->key()==Qt::Key_Right||event->key()==Qt::Key_Left||event->key()==Qt::Key_Z||
        event->key()==Qt::Key_Up||event->key()==Qt::Key_Down    )
    {
        this->myself->step->stop();
this->myself->changeCloth(":/res/RES/1me.png");
        return;
    }
    return QMainWindow::keyReleaseEvent(event);

}
