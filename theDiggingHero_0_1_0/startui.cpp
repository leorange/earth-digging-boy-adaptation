#include "startui.h"
#include "ui_startui.h"
#include<QPainter>
#include<QTimer>
#include<QAction>
#include<mypushbutton.h>
#include<QDebug>


StartUi::StartUi(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::StartUi)
{bgm=new QSound(":/res/RES/bgm1.wav",this);

bgm->play();



    ui->setupUi(this);

    this->setFixedSize(600,600);
    this->setWindowIcon(QPixmap(":/res/RES/b5efa3e82300ed987d52f31ec973ab3.png"));
    this->setWindowTitle("The Digging Hero");


    //设置界面按钮

    MyPushButton * startBtn=new MyPushButton(":/res/RES/start1.png",":/res/RES/start2.png");
    startBtn->setParent(this);
    MyPushButton *exitBtn=new MyPushButton(":/res/RES/exit1.png",":/res/RES/exit2.png");
    exitBtn->setParent(this);



    startBtn->move(this->width()*0.3-startBtn->width()*0.5,this->height()*0.85-startBtn->height()*0.5);


    connect(startBtn,&MyPushButton::clicked,[=](){

        startBtn->zoom1();
        startBtn->zoom2();
            this->playUi=new PlayScene(this);
        QTimer::singleShot(600,this,[=](){
bgm->stop();
            this->close();
            playUi->setGeometry(this->geometry());
            playUi->show();
        });

    });

    //退出按钮的设置
    exitBtn->move(this->width()*0.7-exitBtn->width()*0.5,this->height()*0.85-exitBtn->height()*0.5);
    connect(exitBtn,&QPushButton::clicked,[=](){
        QTimer::singleShot(500,this,[=](){
            this->close();
        });

    });

ui->actionQuit->close();

}
StartUi::~StartUi()
{
    delete ui;
}

void StartUi::paintEvent(QPaintEvent *)
{
    //创建画家，指定绘图设备
    QPainter painter(this);
    //创建QPixmap对象
    QPixmap pix;
    //加载图片
    pix.load(":/res/RES/startui.png");
    //绘制背景图
    painter.drawPixmap(0,0,this->width(),this->height(),pix);

}

