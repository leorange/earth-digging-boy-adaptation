#include "startui.h"
#include "ui_startui.h"
#include<QPainter>
#include<QTimer>
#include<QAction>
#include<mypushbutton.h>
#include<QDebug>

StartUi::StartUi(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::StartUi)
{




    ui->setupUi(this);
    this->playUi=new PlayScene(this);
    this->setFixedSize(600,600);
    this->setWindowIcon(QPixmap(":/res/RES/b5efa3e82300ed987d52f31ec973ab3.png"));
    this->setWindowTitle("The Digging Hero");


    //设置界面按钮

    MyPushButton * startBtn=new MyPushButton(":/res/RES/MenuSceneStartButton.png");
    startBtn->setParent(this);
    ui->actionQuit->move(this->width()*0.7-ui->actionQuit->width()*0.5,this->height()*0.85-ui->actionQuit->height()*0.5);
    startBtn->move(this->width()*0.3-startBtn->width()*0.5,this->height()*0.85-startBtn->height()*0.5);
    connect(startBtn,&MyPushButton::clicked,[=](){

        startBtn->zoom1();
        startBtn->zoom2();
        QTimer::singleShot(500,this,[=](){

            this->hide();
            playUi->setGeometry(this->geometry());
            playUi->show();
        });

    });
    connect(ui->actionQuit,&QPushButton::clicked,[=](){
        QTimer::singleShot(500,this,[=](){
            this->close();
        });

    });





}



StartUi::~StartUi()
{
    delete ui;
}

void StartUi::paintEvent(QPaintEvent *)
{
    //创建画家，指定绘图设备
    QPainter painter(this);
    //创建QPixmap对象
    QPixmap pix;
    //加载图片
    pix.load(":/res/RES/startui.png");
    //绘制背景图
    painter.drawPixmap(0,0,this->width(),this->height(),pix);

}

