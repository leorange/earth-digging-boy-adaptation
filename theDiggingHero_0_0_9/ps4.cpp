#include "ps4.h"
#include "ui_ps4.h"
#include<QTimer>
#include<QDebug>
#include<QPixmap>
#include<QPainter>
#include<QPropertyAnimation>
#include<QDebug>
ps4::ps4(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::ps4)
{
    ui->setupUi(this);
   ui->setupUi(this);
ui->scoree->move(450,15);
   this->score =0;
   this->grabKeyboard();
   this->setFixedSize(600,600);
   this->setWindowIcon(QPixmap(":/res/RES/b5efa3e82300ed987d52f31ec973ab3.png"));
   this->setWindowTitle("The Digging Hero");
   //积分
   score=0;
   scoreLabel=new QLabel(this);
   scoreLabel->move(500,10);
   QTimer *scoreTimer=new QTimer(this);
   scoreTimer->start(200);
   connect(scoreTimer,&QTimer::timeout,[=](){

       scoreLabel->setText(QString::number(score));
   });
//绘制背景音乐
   bgm=new QSound(":/res/RES/bgm3.wav",this);
   bgm->setLoops(-1);
   bgm->play();
//渲染土壤背景
//this->paintBackofSoil();



   losem=new QSound(":/res/RES/losem.wav",this);

      lose=new QLabel;
     if(! losepix.load(":/res/RES/gameover.png"))
     {
     qDebug()<<"failed to load gameover!";

     };
     losepix=losepix.scaled(200,100);
     lose->setGeometry(0,0,losepix.width(),losepix.height());

      lose->setPixmap(losepix);
              lose->setParent(this);
        lose->move(0.5*(600-200),-100);
        exitt=new MyPushButton(":/res/RES/exit1.png",":/res/RES/exit1.png");
               exitt->setParent(this);
               exitt->move(300,-100);
               connect(exitt,&MyPushButton::clicked,[=](){
                   qDebug()<<"close~!!";

                   this->close();
               });


//绘制土壤矿土

   for(int i=0;i<20;i++)
   {
       for(int j=0;j<20;j++)
       {


           soil *s=new soil("0");
           s->setParent(this);
           s->move(30*i,30*j);
           s->posX=i;
           s->posY=j;
           this->tudi[i][j]=s;
       }
   }
this->paintSoilandMine();

   //创建玩家
myself=new Player(":/res/RES/1me.png");
myself->setParent(this);
myself->move(300,0);//他的脚在(x，y+50);
//创建一个NPC
Tutu=new NPC(":/res/RES/Tutu.PNG");
Tutu->setParent(this);
Tutu->move(300,0);


//玩家血条


this->paintBloodBar();


//    实时更新
QTimer *timer=new QTimer(this);//设定计时器
timer->start(50);//计时器时间是0.5s
connect(timer,&QTimer::timeout,[=](){

    int ii,jj;
    ii=(myself->x())/30;
    jj=(myself->y()+50)/30;
    if(tudi[ii][jj]->soilImg==":/res/RES/1soil.png"|tudi[ii][jj]->soilImg=="0")
    {
           int xx=myself->x();
           int yy=myself->y();
           myself->move(xx,yy);
           xx=myself->x();
           yy=myself->y()+2;
           myself->move(xx,yy);
           xx=myself->x();
           yy=myself->y()+2;
           myself->move(xx,yy);
           xx=myself->x();
           yy=myself->y()+2;
           myself->move(xx,yy);
           xx=myself->x();
           yy=myself->y()+2;
           myself->move(xx,yy);
           xx=myself->x();
           yy=myself->y()+2;
           myself->move(xx,yy);
    }

});//信号连接槽，发出信号的是timer，信号是timeout,实时判断是否掉落并且安排掉落,实时监督NPC与玩家的位置并安排攻击
this-> createNPC();
QTimer *TutuTimer=new QTimer(this);
TutuTimer->start(50);
connect(TutuTimer,&QTimer::timeout,[=](){

    //对于图图的重力效应
if(Tutu->y()>600)
{

   Tutu->shengming-=100;

}
         int mm=(Tutu->x())/30;
         int nn=(Tutu->y()+50)/30;
         if(tudi[mm][nn]->soilImg==":/res/RES/1soil.png"||tudi[mm][nn]->soilImg=="0")
         {


                int xx=Tutu->x();
                int yy=Tutu->y();
                xx=Tutu->x();
                yy=Tutu->y()+2;
                Tutu->move(xx,yy);
                xx=Tutu->x();
                yy=Tutu->y()+2;
                Tutu->move(xx,yy);
                xx=Tutu->x();
                yy=Tutu->y()+2;
               Tutu->move(xx,yy);
                xx=Tutu->x();
                yy=Tutu->y()+2;
                Tutu->move(xx,yy);
                xx=Tutu->x();
                yy=Tutu->y()+2;
                Tutu->move(xx,yy);
         }

         if(Tutu->shengming<=0)
         {

             Tutu->close();

             ui->npcBlood->close();
             ui->npcName->close();
         }

});

//连接NPC的攻击与玩家掉血
QTimer *TutuattackTimer=new QTimer(this);
TutuattackTimer->start(1000);
connect(TutuattackTimer,&QTimer::timeout,[=](){
    if(((Tutu->x()-myself->x()<=40)&&(Tutu->x()-myself->x()>=-40))&&(((Tutu->y()-myself->y()<=30)&&(Tutu->y()-myself->y()>=-30))))
    {
        myself->beAttacked();

    }
});

connect(myself,&Player::attack,[=](){
    if(((Tutu->x()-myself->x()<=40)&&(Tutu->x()-myself->x()>=-40))&&(((Tutu->y()-myself->y()<=30)&&(Tutu->y()-myself->y()>=-30))))
    {
        this->Tutu->beAttack();
        QSound beattack(":/res/RES/daji.wav",this);
        beattack.play();
        qDebug()<<Tutu->shengming;

    }

});
this->movingNPC();
///////开启下一关
this->nextlevel();
//this->next=new ps1;
//QTimer *playthenext=new QTimer(this);
//playthenext->start(500);
//connect(playthenext,&QTimer::timeout,[=](){

//if(this->score>=500)
//{

//next->setGeometry(this->geometry());
//this->hide();
//next->show();
//}

//});

this->gameover();

}

ps4::~ps4()
{
    delete ui;
}
void ps4::keyPressEvent(QKeyEvent *ev)
{
           qDebug()<<"keyEvent in 3";
    if(ev->key()==Qt::Key_Right)
    {


        int xx=this->myself->x();
        int yy=this->myself->y();

        if(xx<590)
        {

            qDebug()<<xx<<"  "<<yy;
            int ii=xx/30;
            int jj=(yy)/30;
            qDebug()<<ii<<" "<<jj;
                if(tudi[ii][jj+1]->soilImg=="0"||tudi[ii][jj+1]->soilImg==":/res/RES/1soil.png")
                {
                    this->myself->move(xx+10,yy);
                    this->myself->step->play();
                this->myself->changeCloth(":/res/RES/r0me.png");
                QTimer::singleShot(1000,this,[=](){
                    if(ev->key()==Qt::Key_Right)
                    {
                this->myself->changeCloth(":/res/RES/r1me.png");
                    }
                });

                }
                else
                {

                    qDebug()<<"begin to dig the right";
                    this->myself->changeCloth(":/res/RES/digR0.png");
                    QTimer::singleShot(800,this,[=](){
                        this->tudi[ii][jj+1]->changeCloth(":/res/RES/1soil.png");
                    });

                    QTimer::singleShot(200,this,[=](){
                         this->myself->changeCloth(":/res/RES/1me.png");
                        this->score+=10;
                    });
                }
        }
        return;

    }
    if(ev->key()==Qt::Key_Left)
    {   int xx=this->myself->x();
        int yy=this->myself->y();
        if(xx>=10)
        {

        qDebug()<<xx<<"  "<<yy;
        int ii=xx/30;
        int jj=(yy)/30;
        qDebug()<<ii<<" "<<jj;
            if(tudi[ii][jj+1]->soilImg=="0"||tudi[ii][jj+1]->soilImg==":/res/RES/1soil.png")
            {
                this->myself->move(xx-10,yy);
                this->myself->step->play();
            this->myself->changeCloth(":/res/RES/l0me.png");
            QTimer::singleShot(1000,this,[=](){
                 if(ev->key()==Qt::Key_Left)
                 {
            this->myself->changeCloth(":/res/RES/l1me.png");
}
            });

            }
            else
            {
                qDebug()<<"begin to dig the left";
                this->myself->changeCloth(":/res/RES/digL0.png");
                QTimer::singleShot(800,this,[=](){
                    this->tudi[ii][jj+1]->changeCloth(":/res/RES/1soil.png");
                });
                    this->score+=10;
            }
        }
        return;
    }
    if(ev->key()==Qt::Key_Up)
    {
        int xx=this->myself->x();
        int yy=this->myself->y()-10;
        int ii=xx/30;
        int jj=(yy)/30;
        if(tudi[ii][jj]->soilImg=="0"|tudi[ii][jj]->soilImg==":/res/RES/1soil.png")
        {
        this->myself->changeCloth(":/res/RES/4me.png");
        this->myself->move(xx,yy);
        }
        return;
    }
    if(ev->key()==Qt::Key_Down)
    {

        int xx=this->myself->x();
        int yy=this->myself->y();
     int ii=xx/30;
     int jj=(yy+50)/30;

     if(this->tudi[ii][jj]->soilImg==":/res/RES/2soil.png")
     {
         this->myself->changeCloth(":/res/RES/3me.png");
         QTimer::singleShot(800,this,[=](){
             this->tudi[ii][jj]->changeCloth(":/res/RES/1soil.png");
             this->score+=10;
         });
     }
        return;
    }
    if(ev->key()==Qt::Key_Space)
    {
        this->myself->beAttacked();
        QSound beattack(":/res/RES/daji.wav",this);
        beattack.play();
        return;
    }
    if(ev->key()==Qt::Key_Z)
    {

        if(((Tutu->x()-myself->x()<=40)&&(Tutu->x()-myself->x()>=-40))&&(((Tutu->y()-myself->y()<=30)&&(Tutu->y()-myself->y()>=-30))))
        {
            this->myself->attack();
            Tutu->beAttack();
            qDebug()<<"myself->attack()";
        }
        qDebug()<<myself->x()<<"  "<<myself->y();
        qDebug()<<Tutu->x()<<"    "<<Tutu->y();
        return;
    }
   return QMainWindow::keyPressEvent(ev);


}
void ps4::paintSky()
{
    QPainter painter(this);
        QPixmap sky;
        sky.load(":/res/RES/loadingUi.png");
    sky=sky.scaled(600,600);
    painter.drawPixmap(0,0,this->width(),this->height(),sky);
}
void ps4::paintBackofSoil(){
    QLabel *label[20][20];
 for(int i=0;i<20;i++)
 {
     for(int j=0;j<20;j++)
     {
         QLabel* labe=new QLabel(this);
         QPixmap tupix;
         tupix.load("0");
//                 (":/res/RES/3soil.png");
//         tupix=tupix.scaled(50,50);
         labe->setGeometry(0,0,30,30);
         labe->setPixmap(tupix);
         labe->move(30*i,30*j);
         label[i][j]=labe;
     }
 }
}
void ps4::paintBloodBar()
{

    ui->npcBlood->move(this->width()-10-ui->npcBlood->width(),40);
    ui->npcName->setText("HuTutu");
      ui->npcName->move(this->width()-10-ui->npcBlood->width()-45,40);

ui->progressBar->move(this->width()-10-ui->npcBlood->width(),65);

    //玩家血量条
     ui->progressBar->setMaximum(100);
     ui->progressBar->setValue(this->myself->shengming);
     ui->npcBlood->setMaximum(100);
     ui->npcBlood->setValue(this->Tutu->shengming);
     QTimer *bloodtime=new QTimer(this);
     bloodtime->start(500);
     connect(bloodtime,&QTimer::timeout,[=](){
         ui->progressBar->setValue(myself->shengming);
         ui->npcBlood->setValue(Tutu->shengming);
     });

}
void ps4::movingNPC()
{

    QTimer *mNTimer=new QTimer(this);
    mNTimer->start(200);
this->t=0;
    this->ans=0;
    connect(mNTimer,&QTimer::timeout,[=](){

        if(Tutu->shengming<=0)
        {
            mNTimer->stop();
        }

        int x=Tutu->x();
        int y=Tutu->y();
        int ii=x/30;
        int jj=(y-10)/30;

            if(this->ans)
            {
                if((tudi[ii][jj+1]->soilImg=="0"||tudi[ii][jj+1]->soilImg==":/res/RES/1soil.png")&&(x>=5))
                {
            this->Tutu->move(x-5,y);
            }
                t--;
            }
            else
            {
                if((tudi[ii][jj+1]->soilImg=="0"||tudi[ii][jj+1]->soilImg==":/res/RES/1soil.png")&&(x<580))
                {
            this->Tutu->move(x+5,y);
                }
                t=t+1;
            }
            if(t>=30)
            {
               this-> ans=1;
            }
            if(t<=0)
            {
                this->ans=0;
            }

    });



}
void ps4::keyReleaseEvent(QKeyEvent *event)
{

    if(event->key()==Qt::Key_Right||event->key()==Qt::Key_Left||event->key()==Qt::Key_Z||
        event->key()==Qt::Key_Up||event->key()==Qt::Key_Down    )
    {
        this->myself->step->stop();
this->myself->changeCloth(":/res/RES/1me.png");
        return;

    }
    return QMainWindow::keyReleaseEvent(event);


}
void ps4::createNPC()
{






    for(int i=0;i<5;i++)
    {
        lee[i]=new QLabel(this);
        leBlood[i]=new QProgressBar(this);
        this->tt[i]=0;
        this->le[i]=new NPC(":/res/RES/smallMor.png");
        le[i]->setParent(this);
        leBlood[i]->setValue(this->le[i]->shengming);
        lee[i]->move(this->width()-lee[i]->width()-10-leBlood[i]->width(),100+20*i);
        leBlood[i]->move(this->width()-10-leBlood[i]->width(),100+20*i);
        NPCattack[i]=new QTimer(this);

    }

    lee[0]->setText("HeroLe");
       lee[1]->setText("乐乐侠");
          lee[2]->setText("MoLe");
             lee[3]->setText("穿着乐乐侠衣服的摩乐乐");
                lee[4]->setText("坏的摩尔");
    QTimer *npc1=new QTimer(this);
    npc1->start(50);
for(int i=0;i<5;i++)
{

    connect(npc1,&QTimer::timeout,[=](){
        //设置重力
        for(int i=0;i<5;i++)
        {
            if(le[i]->y()>600)
            {

               le[i]->shengming-=100;

            }
                     int mm=(le[i]->x())/30;
                     int nn=(le[i]->y()+50)/30;
                     if(tudi[mm][nn]->soilImg==":/res/RES/1soil.png"||tudi[mm][nn]->soilImg=="0")
                     {


                            int xx=le[i]->x();
                            int yy=le[i]->y();
                            xx=le[i]->x();
                            yy=le[i]->y()+2;
                            le[i]->move(xx,yy);
                            xx=le[i]->x();
                            yy=le[i]->y()+2;
                            le[i]->move(xx,yy);
                            xx=le[i]->x();
                            yy=le[i]->y()+2;
                           le[i]->move(xx,yy);
                            xx=le[i]->x();
                            yy=le[i]->y()+2;
                            le[i]->move(xx,yy);
                            xx=le[i]->x();
                            yy=le[i]->y()+2;
                            le[i]->move(xx,yy);
                     }

                     if(le[i]->shengming<=0)
                     {

                         le[i]->close();

                         leBlood[i]->close();
                         lee[i]->close();

                         NPCattack[i]->stop();
                          move[i]->stop();


                     }





        }
        //跟随血量
        for(int i=0;i<5;i++)
        {
            leBlood[i]->setValue(this->le[i]->shengming);
        }
    });

}
    //攻击
    for(int i=0;i<5;i++)
    {
           NPCattack[i]->start(1000);
    connect(NPCattack[i],QTimer::timeout,[=](){
        if(((le[i]->x()-myself->x()<=40)&&(le[i]->x()-myself->x()>=-40))&&(((le[i]->y()-myself->y()<=30)&&(le[i]->y()-myself->y()>=-30))))
        {
            myself->beAttacked();

        }
    });



    }

    //被攻击
    for(int i=0;i<5;i++)
    {
    connect(myself,&Player::attack,[=](){
        if(((le[i]->x()-myself->x()<=40)&&(le[i]->x()-myself->x()>=-40))&&(((le[i]->y()-myself->y()<=30)&&(le[i]->y()-myself->y()>=-30))))
        {
            this->le[i]->beAttack();
            QSound beattack(":/res/RES/daji.wav",this);
            beattack.play();
            qDebug()<<le[i]->shengming;

        }

    });
    }
    //会走路吗？
    tt[0]=1;
    tt[1]=18;
    tt[2]=29;
    tt[3]=8;
    tt[4]=12;
    for(int i=0;i<5;i++)
    {

        move[i]=new QTimer(this);
        move[i]->start(200);
            anss[i]=1;
        connect(move[i],&QTimer::timeout,[=](){


            int x=le[i]->x();
            int y=le[i]->y();
            int ii=x/30;
            int jj=(y-10)/30;

                if(this->anss[i])
                {
                    if((tudi[ii][jj+1]->soilImg=="0"||tudi[ii][jj+1]->soilImg==":/res/RES/1soil.png")&&(x>=5))
                    {
                this->le[i]->move(x-5,y);
                    }
                    tt[i]--;
                }
                else
                {
                    if((tudi[ii][jj+1]->soilImg=="0"||tudi[ii][jj+1]->soilImg==":/res/RES/1soil.png")&&(x<580))
                    {
                this->le[i]->move(x+5,y);
                    }
                     tt[i]=tt[i]+1;
                }



                if( tt[i]>=30)
                {
                   this-> anss[i]=1;
                }
                if( tt[i]<=0)
                {
                    this->anss[i]=0;
                }

        });
    }
   //设定初始位置
    le[0]->move(3*30,8*30);
    le[1]->move(15*30,12*30);
    le[2]->move(2*30,3*30);
    le[3]->move(16*30,17*30);
    le[4]->move(2*30,17*30);




}
void ps4::nextlevel()
{


    QTimer *playthenext=new QTimer(this);
    playthenext->start(500);
    connect(playthenext,&QTimer::timeout,[=](){

    if(this->score>=3500)
    {  bgm->stop();
        delete bgm;
        this->next=new QLabel(this);
        next->setText("WIN!");
        next->move(200,400);
        next->show();
playthenext->stop();
    next->setGeometry(this->geometry());
    next->show();
    }
    });


}
void ps4::paintEvent(QPaintEvent *ev)
{

    //创建画家，指定绘图设备
        QPainter painter(this);
        //创建QPixmap对象
        QPixmap pix=QPixmap(":/res/RES/bg2.jpg").scaled(this->size());
        //加载图片

        //绘制背景图
        painter.drawPixmap(0,0,this->width(),this->height(),pix);


    return ;




}
void ps4::paintSoilandMine()
{


    for(int i=0;i<20;i++)
    {
        for(int j=0;j<20;j++)
        {
this->tudi[i][j]->changeCloth("0");
            if(i>=13&&i<17&&j>=0&&j<20)
            {
                this->tudi[j][i]->changeCloth(":/res/RES/2soil.png");
            }
            if(i>=18&&i<=19&&j>=7&&j<=12)
            {
                 this->tudi[j][i]->changeCloth(":/res/RES/2soil.png");
            }
            if(i>=10&&i<17&&j>=0&&j<=7)
            {
                this->tudi[j][i]->changeCloth(":/res/RES/2soil.png");
            }
            if(j>14&&j<=17&&i<=19)
            {
                this->tudi[i][j]->changeCloth(":/res/RES/2soil.png");
            }

//            if((i==9||i==10)&&j>=0&&j<=5)
//            {
//                this->tudi[i][j]->changeCloth(":/res/RES/2soil.png");
//            }
            if((i==10||i==11||i==12)&&j>=7&&j<=13)
            {
                 this->tudi[i][j]->changeCloth(":/res/RES/2soil.png");
            }
            if((i==11||i==12)&&j>=14)
            {
                         this->tudi[i][j]->changeCloth(":/res/RES/2soil.png");
            }

            if(j==19)
            {
                    this->tudi[i][j]->changeCloth(":/res/RES/9soil.png");
            }

        }


    }



}
void ps4::gameover()
{

    QTimer *timee=new QTimer(this);
    timee->start(200);
    connect(timee,QTimer::timeout,[=](){

        qDebug()<<myself->shengming;

        if(myself->shengming<=2)
        {

            timee->stop();

            qDebug()<<"Game Over！！";

QPropertyAnimation *anii1=new QPropertyAnimation
        (lose,"geometry");
anii1->setDuration(1000);
anii1->setStartValue(QRect(lose->x(),lose->y(),lose->width(),lose->height()));
anii1->setEndValue(QRect(lose->x(),lose->y()+200,lose->width(),lose->height()));

anii1->setEasingCurve(QEasingCurve::OutBounce);
anii1->start();
            bgm->stop();
            losem->play();

        exitt->move(400,300);
        QTimer::singleShot(2000,this,[=](){

            this->close();

        });


        }





    });




}

