#ifndef STARTUI_H
#define STARTUI_H

#include <QMainWindow>
#include<playscene.h>
#include<QSound>
#include<QMediaPlayer>
QT_BEGIN_NAMESPACE
namespace Ui { class StartUi; }
QT_END_NAMESPACE

class StartUi : public QMainWindow
{
    Q_OBJECT

public:
    StartUi(QWidget *parent = nullptr);
    ~StartUi();
    PlayScene *playUi;

    QSound *bgm;
void paintEvent(QPaintEvent *);
private:
    Ui::StartUi *ui;

};
#endif // STARTUI_H
